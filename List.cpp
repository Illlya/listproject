#include <iostream>
#include "List.hpp"
using namespace std;

auto List::printlist() const -> void
{
	Node* p = first;
	while (p->next != nullptr) {
		cout << p->data << " -> ";
		p = p->next;
	}
	cout << p->data << endl;
}
auto List::insert(int n) -> void {
	Node* p = first;
	if (p == nullptr) {
		this->first = new Node{ n, nullptr };
	}
	else {
		while (p->next != nullptr) {
			p = p->next;
		}
		p->next = new Node{ n,nullptr };
	}
}

auto List::remove(int n) -> bool {
	bool indicator = false;
	Node* p = first;
	if (p == nullptr) cout << "Список пуст" << endl;
	else {
		if (first->data == n) {
			first = first->next;
			indicator = true;
			delete p;
			p = first;
		}
		if (p != nullptr)
			while ((p->next != nullptr) && (!indicator)) {
				if (p->next->data == n) {
					if (p->next->next == nullptr) {
						delete p->next;
						p->next = nullptr;
					}
					else {
						Node* q = p->next;
						p->next = p->next->next;
						delete q;
					}
					indicator = true;
				}
				if (p->next != nullptr) p = p->next;
			}
	}
	return indicator;
}

auto List::empty_list() const -> bool {
	if (this->first == nullptr) return true;
	else return false;
}

auto List::search(int value) const -> bool {
	bool indicator = false;
	int number = 0;
	Node* p = first;
	while (p != nullptr) {
		if (p->data == value) {
			indicator = true;
			cout << number << " ";
		}
		p = p->next;
		number++;
	}
	return indicator;
}

auto List::replacement(int position, int value) const -> bool {
	int i = 0;
	Node* p = first;
	while ((i != position) && (p != nullptr)) {
		p = p->next;
		i++;
	}
	if (p == nullptr)
		return false;
	else {
		p->data = value;
		return true;
	}
}

void print_menu() {
	cout << "1.Распечатать список" << endl
		<< "2.Добавить элементы в список" << endl
		<< "3.Удалить элемент" << endl
		<< "4.Найти позиции элементов" << endl
		<< "5.Заменить элемент на другой" << endl
		<< "6.Отсортировать элементы списка" << endl
		<< "7.Завершить работу программы" << endl;
}
